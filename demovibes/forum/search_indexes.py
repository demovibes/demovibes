from haystack import indexes
from forum.models import Post


class PostIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    render = indexes.CharField(indexed=False, use_template=True)

    def get_model(self):
        return Post

    def index_queryset(self):
        return self.get_model().objects.filter(thread__forum__is_private=False)

    def get_updated_field(self):
        return "edited"
