from django.urls import path
from . import views
from .feeds import RssForumFeed, AtomForumFeed


feed_dict = {"rss": RssForumFeed, "atom": AtomForumFeed}

app_name = "forum"
urlpatterns = [
    path("", views.ForumsView.as_view(), name="index"),
    path("thread/<int:thread>/", views.thread, name="view_thread"),
    path("edit/<int:post_id>/", views.edit),
    path("subscriptions/", views.updatesubs, name="subscriptions"),
    path("<slug:slug>/", views.forum, name="thread_list"),
    path("<path:path>/<slug:slug>/", views.forum, name="subforum_thread_list"),
    # path("(?P<url>(rss|atom).*)/", Feed(), {'feed_dict': feed_dict}),
]
