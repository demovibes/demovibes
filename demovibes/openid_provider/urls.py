from django.conf.urls import url
from openid_provider import views

urlpatterns = [
    url(r"^$", views.openid_server, name="openid-provider-root"),
    url(r"^decide/$", views.openid_decide, name="openid-provider-decide"),
    url(r"^xrds/$", views.openid_xrds, name="openid-provider-xrds"),
    url(
        r"^(?P<id>.*)/$",
        views.openid_xrds,
        {"identity": True},
        name="openid-provider-identity",
    ),
]
