from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.db import models
from django.db.models.signals import post_save


class OpenID(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    openid = models.CharField(max_length=200, blank=True, unique=True)
    default = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse("openid-provider-identity", args=[self.openid])

    class Meta:
        verbose_name = _("OpenID")
        verbose_name_plural = _("OpenIDs")
        ordering = ["openid"]

    def __unicode__(self):
        return "%s|%s" % (self.user.username, self.openid)

    def save(self, *args, **kwargs):
        if self.openid in ["", "", None]:
            from hashlib import sha1
            import random
            import base64

            sha = sha1()
            sha.update(unicode(self.user.username).encode("utf-8"))
            sha.update(str(random.random()))
            value = str(base64.b64encode(sha.digest()))
            value = value.replace("/", "").replace("+", "").replace("=", "")
            self.openid = value
        super(OpenID, self).save(*args, **kwargs)
        if self.default:
            self.user.openid_set.exclude(pk=self.pk).update(default=False)


class TrustedRoot(models.Model):
    openid = models.ForeignKey(OpenID, on_delete=models.CASCADE)
    trust_root = models.CharField(max_length=200)

    def __unicode__(self):
        return unicode(self.trust_root)


def add_openid(sender, **kwargs):
    if kwargs["created"]:
        try:
            user = kwargs["instance"]
            user.openid_set.create(openid=user.username)
        except:
            pass


post_save.connect(add_openid, sender=User)
