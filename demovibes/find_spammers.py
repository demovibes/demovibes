def find_spammers(User, limit=4000, list_webpages=False):
    a = []
    for x in User.objects.all().order_by("-id")[:limit]:
        if (
            (
                "[url=h" in x.userprofile.info
                or (list_webpages and x.userprofile.web_page)
            )
            and x.favorite_set.count() == 0
            and not x.userprofile.have_artist()
        ):
            a.append(x)
    return a


def get_spammers(User, limit=4000, list_webpages=False):
    a = []
    for x in find_spammers(User, limit, list_webpages):
        print(
            "\n\n",
            x,
            x.id,
            x.userprofile.web_page,
            "\n",
            x.userprofile.info,
            x.userprofile.country,
        )
        if input("Add to list? (0 for no, 1 for yes) "):
            a.append(x)
    return a
