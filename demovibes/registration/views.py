import datetime
import hashlib
import logging

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import authenticate, login
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, Context, loader
from django.urls import reverse
from django.views.generic.base import TemplateView

from mybaseview import MyBaseView
from webview.models import OnelinerMuted, Userprofile
from .forms import RegistrationFormNoFreeEmail
from .models import RegistrationProfile


L = logging.getLogger("dv.registration")


class RegistrationFormNoFreeEmailFromSetting(RegistrationFormNoFreeEmail):
    bad_domains = getattr(settings, "BAD_EMAIL_DOMAINS", [])


def send_email(mail_to, subject, content):
    email = EmailMessage(
        subject=subject,
        body=content,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[mail_to],
    )
    email.send(fail_silently=True)


@staff_member_required
def activate2(request):
    aq = RegistrationProfile.objects.filter(
        user__is_active=False, activation_key="ALREADY_ACTIVATED"
    ).order_by("-id")
    c = {"accounts": aq}
    if request.method == "POST":
        profileid = request.POST.get("okid")
        a_activate = request.POST.get("doactivate")
        a_remove = request.POST.get("doremove")
        p = RegistrationProfile.objects.get(id=profileid)
        u = p.user
        # Creating a userprofile if needed
        try:
            up = u.userprofile
        except:
            up = Userprofile()
            up.user = u
            up.last_ip = "127.0.0.1"
            up.save()
        if a_activate:
            u.is_active = True
            u.save()
            up.log(request.user, "Account activated")
            send_email(
                u.email, "Account active", "Your account on Nectarine is now active :)"
            )
        if a_remove:
            p.activation_key = "DEACTIVATED"
            p.save()
            up.log(request.user, "Account Denied")
        HttpResponseRedirect(reverse("registration_activate2"))
    context = RequestContext(request)
    return render(request, "registration/activate2.html", c)


def activate(
    request,
    activation_key,
    template_name="registration/activate.html",
    extra_context=None,
):
    """
    Activate a ``User``'s account from an activation key, if their key
    is valid and hasn't expired.

    By default, use the template ``registration/activate.html``; to
    change this, pass the name of a template as the keyword argument
    ``template_name``.

    **Required arguments**

    ``activation_key``
       The activation key to validate and use for activating the
       ``User``.

    **Optional arguments**

    ``extra_context``
        A dictionary of variables to add to the template context. Any
        callable object in this dictionary will be called to produce
        the end result which appears in the context.

    ``template_name``
        A custom template to use.

    **Context:**

    ``account``
        The ``User`` object corresponding to the account, if the
        activation was successful. ``False`` if the activation was not
        successful.

    ``expiration_days``
        The number of days for which activation keys stay valid after
        registration.

    Any extra variables supplied in the ``extra_context`` argument
    (see above).

    **Template:**

    registration/activate.html or ``template_name`` keyword argument.

    """
    activation_key = activation_key.lower()  # Normalize before trying anything with it.
    account = RegistrationProfile.objects.activate_user(
        activation_key
    )  # Returns User if successful, otherwise a bool

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    if type(account).__name__ == "bool":
        # This returned something other than a user profile. In future, if this
        # Returns FALSE, we know there was a problem. For now, just bail out.
        return render(
            request,
            template_name,
            {"expiration_days": settings.ACCOUNT_ACTIVATION_DAYS},
        )

    # Inform admin on new user registration?
    email_admin = getattr(
        settings, "ADMIN_NOTIFY_ON_NEW_USER", 0
    )  # Disabled by default
    email_user = getattr(settings, "USER_SEND_CONF_OK", 0)  # Disabled by default

    if email_admin > 0:
        try:
            mail_to = settings.ADMIN_EMAIL
            mail_tpl = loader.get_template("registration/t/new_registrant.txt")
            site = Site.objects.get_current()

            c = Context(
                {
                    "site": site,
                    "username": account.username,
                    "email": account.email,
                }
            )
            email = EmailMessage(
                subject="[" + site.name + "] New User Registration Activated!",
                body=mail_tpl.render(c),
                from_email=mail_to,  # Will always be the same address
                to=[mail_to],
            )
            email.send(fail_silently=True)
        except:
            # Failed to process the email request for some reason
            pass

    if email_user > 0:
        try:
            mail_to = account.email
            mail_tpl = loader.get_template("registration/t/welcome.txt")
            site = Site.objects.get_current()

            c = Context(
                {
                    "site": site,
                    "username": account.username,
                }
            )
            email = EmailMessage(
                subject="[" + site.name + "] Your Account Is Now Active!",
                body=mail_tpl.render(c),
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[mail_to],
            )
            email.send(fail_silently=True)
        except:
            # Failed to process the email request for some reason
            pass

    return render(
        request,
        template_name,
        {"account": account, "expiration_days": settings.ACCOUNT_ACTIVATION_DAYS},
    )


def register(
    request,
    success_url=None,
    form_class=RegistrationFormNoFreeEmailFromSetting,
    profile_callback=None,
    template_name="registration/registration_form.html",
    extra_context=None,
):
    """
    Allow a new user to register an account.

    Following successful registration, issue a redirect; by default,
    this will be whatever URL corresponds to the named URL pattern
    ``registration_complete``, which will be
    ``/accounts/register/complete/`` if using the included URLConf. To
    change this, point that named pattern at another URL, or pass your
    preferred URL as the keyword argument ``success_url``.

    By default, ``registration.forms.RegistrationForm`` will be used
    as the registration form; to change this, pass a different form
    class as the ``form_class`` keyword argument. The form class you
    specify must have a method ``save`` which will create and return
    the new ``User``, and that method must accept the keyword argument
    ``profile_callback`` (see below).

    To enable creation of a site-specific user profile object for the
    new user, pass a function which will create the profile object as
    the keyword argument ``profile_callback``. See
    ``RegistrationManager.create_inactive_user`` in the file
    ``models.py`` for details on how to write this function.

    By default, use the template
    ``registration/registration_form.html``; to change this, pass the
    name of a template as the keyword argument ``template_name``.

    **Required arguments**

    None.

    **Optional arguments**

    ``form_class``
        The form class to use for registration.

    ``extra_context``
        A dictionary of variables to add to the template context. Any
        callable object in this dictionary will be called to produce
        the end result which appears in the context.

    ``profile_callback``
        A function which will be used to create a site-specific
        profile instance for the new ``User``.

    ``success_url``
        The URL to redirect to on successful registration.

    ``template_name``
        A custom template to use.

    **Context:**

    ``form``
        The registration form.

    Any extra variables supplied in the ``extra_context`` argument
    (see above).

    **Template:**

    registration/registration_form.html or ``template_name`` keyword
    argument.

    """
    if request.method == "POST":
        userip = request.META["REMOTE_ADDR"]
        r = OnelinerMuted.objects.filter(
            ip_ban=userip, muted_to__gt=datetime.datetime.now()
        )
        if r:
            d = {
                "reason": r[0].reason,
                "time": r[0].muted_to,
            }
            return render(request, "webview/muted.html", {"muted": d})
        form = form_class(data=request.POST, files=request.FILES)
        """
        cform = captcha.get_form(forms, request.POST)
        """
        if form.is_valid():
            new_user = form.save(profile_callback=profile_callback)
            return HttpResponseRedirect(success_url or reverse("registration_complete"))
        """if form.is_valid():
            if cform.is_valid():
                new_user = form.save(profile_callback=profile_callback)
                # success_url needs to be dynamically generated here; setting a
                # a default value using reverse() will cause circular-import
                # problems with the default URLConf for this application, which
                # imports this file.
                L.info("New user %s - gave captcha answer %s, IP %s", new_user.username, cform.cleaned_data["answer"], userip)
                return HttpResponseRedirect(success_url or reverse('registration_complete'))
            else:
                a = request.POST.get("answer")
                if a:
                    L.info("Captcha failed - answer was '%s'. IP %s", a, userip)
        """

    else:
        form = form_class()
        """
        cform = captcha.get_form(forms)
        """

    """
    cform.auto_set_captcha()
    L.info("Setting captcha %s", cform.fields['answer'].label)
    """

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render(request, template_name, {"form": form})


class RegistrationCompletePageView(TemplateView):
    template_name = "registration/registration_complete.html"


class Login(MyBaseView):
    template = "registration/login.html"

    MAX_FAILS_PER_HOUR = getattr(settings, "MAX_FAILED_LOGINS_PER_HOUR", 5)

    def pre_view(self):
        self.context["next"] = self.request.GET.get("next", "")
        self.context["username"] = self.request.GET.get("username", "")
        self.context["error"] = ""

    def check_limit(self, keys):
        for key in keys:
            if cache.get(key, 0) > self.MAX_FAILS_PER_HOUR:
                return True
        return False

    def add_to_limit(self, keys):
        for key in keys:
            if cache.get(key, None) == None:
                cache.set(key, 1, 60 * 60)
            else:
                cache.incr(key)

    def POST(self):
        ip = self.request.META.get("REMOTE_ADDR")

        username = self.request.POST.get("username", "")
        password = self.request.POST.get("password", "")

        key1 = hashlib.md5(("loginfail" + username).encode("utf-8")).hexdigest()
        key2 = hashlib.md5(("loginfail" + ip).encode("utf-8")).hexdigest()
        if self.check_limit((key1, key2)):
            self.context["error"] = _(
                "Too many failed logins. Please wait an hour before trying again."
            )
            return False

        next = self.request.POST.get("next", False)

        if not username or not password:
            self.context["error"] = _("You need to supply a username and password")
            return

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(self.request, user)
                return self.redirect(next or "webview:root")
            else:
                self.context["error"] = _("I'm sorry, your account have been disabled.")
        else:
            self.add_to_limit((key1, key2))
            self.context["error"] = _(
                "I'm sorry, the username or password seem to be wrong."
            )
