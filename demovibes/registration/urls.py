from django.urls import path, re_path
from django.contrib.auth import views as auth_views
from . import views


app_name = "registration"
urlpatterns = [
    path("register/", views.register, name="register"),
    path(
        "register/complete/",
        views.RegistrationCompletePageView.as_view(),
        name="complete",
    ),
    path("login/", views.Login(), name="login"),
    # Activation keys get matched by \w+ instead of the more specific
    # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
    # that way it can return a sensible "invalid key" message instead of a
    # confusing 404.
    re_path(r"activate/(?P<activation_key>\w+)/", views.activate, name="activate"),
    path("activates/", views.activate2, name="activate2"),
    path(
        "password/change/",
        auth_views.PasswordChangeView.as_view(),
        name="auth_password_change",
    ),
    path(
        "password/change/done/",
        auth_views.PasswordChangeDoneView.as_view(),
        name="auth_password_change_done",
    ),
    path(
        "password/reset/",
        auth_views.PasswordResetView.as_view(),
        name="auth_password_reset",
    ),
    re_path(
        r"password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="auth_password_reset_confirm",
    ),
    path(
        "password/reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="auth_password_reset_done",
    ),
    path(
        "password/reset/complete/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="auth_password_reset_complete",
    ),
]
