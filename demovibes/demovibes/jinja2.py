from django.conf import settings
from django.core.cache import cache
from django.urls import reverse, NoReverseMatch
from django.utils import translation
from django.utils.translation import gettext, ngettext
from django.template import defaultfilters
from django.templatetags.static import static
from jinja2 import Environment, escape
from webview.templatetags import dv_extend


def dummy(dummystuff):
    return "Dummy for %s" % dummystuff


def mkstr(*args):
    args = [str(x) for x in args]
    return "".join(args)


def url(view_name, *args, **kwargs):
    try:
        return reverse(view_name, args=args, kwargs=kwargs)
    except NoReverseMatch:
        try:
            project_name = settings.SETTINGS_MODULE.split(".")[0]
            return reverse(project_name + "." + view_name, args=args, kwargs=kwargs)
        except NoReverseMatch:
            return ""


def truncatewords(string, number):
    return " ".join(string.split()[:number])


def nbspize(text):
    import re

    return re.sub(r"\s", "&nbsp;", text.strip())


def get_lang():
    return translation.get_language()


def timesince(date):
    from django.utils.timesince import timesince

    return timesince(date)


def timeuntil(date):
    from django.utils.timesince import timesince
    from datetime import datetime

    return timesince(datetime.now(), datetime(date.year, date.month, date.day))


def mksafe(arg):
    """
    Force escaping of html

    First turn it into a Markup() type with escape, then force it into string again,
    so other modifications to the string won"t be automatically escaped.
    """
    return str(escape(arg))


def environment(**options):
    env = Environment(**options, extensions=["jinja2.ext.i18n"])
    env.install_gettext_callables(gettext=gettext, ngettext=ngettext, newstyle=True)
    env.globals.update(
        {
            "cache": cache,
            "url": reverse,
            "staticc": static,
            "mkstr": mkstr,
            "dummy": dummy,
            "dv": dv_extend,
            "STATIC_URL": settings.STATIC_URL,
        }
    )
    env.filters.update(
        {
            "time": defaultfilters.time,
            "date": defaultfilters.date,
            "pluralize": defaultfilters.pluralize,
            "timesince": timesince,
            "timeuntil": timeuntil,
            "floatformat": defaultfilters.floatformat,
            "linebreaks": defaultfilters.linebreaks,
            "linebreaksbr": defaultfilters.linebreaksbr,
            "smileys": dv_extend.smileys,
            "smileys_oneliner": dv_extend.smileys_oneliner,
            "oneliner_mediaparse": dv_extend.oneliner_mediaparse,
            "bbcode_oneliner": dv_extend.bbcode_oneliner,
            "bbcode": dv_extend.bbcode,
            "dv_urlize": dv_extend.dv_urlize,
            "mksafe": mksafe,
            "ankhalize": dv_extend.ankhalize,
            "antiankh": dv_extend.antiankh_filter,
            "custom_filters": dv_extend.custom_filters,
            "restricted_smileys": dv_extend.smileys_restricted,
            "truncatewords": truncatewords,
        }
    )

    return env
