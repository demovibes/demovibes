from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

from webview.views import log_out


urlpatterns = [
    path("", RedirectView.as_view(url="/demovibes/")),
    path("admin/", admin.site.urls),
    path("accounts/", include("registration.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("accounts/logout/", log_out, name="log_out"),
    path("demovibes/", include("webview.urls")),
    path("forum/", include("forum.urls")),
    path("search/", include("search.urls")),
    # path("", 'django.views.generic.simple.redirect_to', {'url': '/static/biebervibes/'}),  # April 1st 2015
    # ~ path("accounts/profile/", RedirectView.as_view(url='/demovibes/')),
    # ~ path("openid/', include('openid_provider.urls')),
]

# Only use this under development! Only for serving media files with dev server!
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
