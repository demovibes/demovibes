import json
from pathlib import Path

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from webview.models import SongLicense


class Command(BaseCommand):
    help = "Imports licenses from json file passed as argument"

    def add_arguments(self, parser):
        parser.add_argument("license_file", type=str)

    def handle(self, *args, **options):
        filepath = Path(options.get("license_file", "licenses.json"))
        if not filepath.exists():
            raise CommandError(f"File {filepath} doesn't exists!")

        licenses = []
        with open(filepath) as file_handler:
            licenses = json.load(file_handler)
        if len(licenses) == 0:
            raise CommandError(f"File contains nothing!")

        for license in licenses:
            try:
                song_license = SongLicense.objects.get(pk=license["id"])
                self.stdout.write(f" Updating existing license: {license['name']}")
                song_license.name = license["name"]
                song_license.description = license["description"]
                song_license.url = license["url"] or ""
                song_license.downloadable = license["downloadable"]
            except SongLicense.DoesNotExist:
                self.stdout.write(f" Creating new license: {license['name']}")
                song_license = SongLicense(
                    name = license["name"],
                    description = license["description"],
                    url = license["url"] or "",
                    downloadable = license["downloadable"],
                )
            # Loading icon file
            with open(filepath.parent / license["icon"], "rb") as icon_file:
                song_license.icon.save(license["icon"], File(icon_file))

            song_license.save()

        self.stdout.write(self.style.SUCCESS("Import finished"))
