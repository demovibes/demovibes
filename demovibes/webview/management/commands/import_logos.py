import json
from pathlib import Path

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from webview.models import Logo


class Command(BaseCommand):
    help = "Imports logos from json file passed as argument"

    def add_arguments(self, parser):
        parser.add_argument("logo_file", type=str)

    def handle(self, *args, **options):
        filepath = Path(options.get("logo_file", "logos.json"))
        if not filepath.exists():
            raise CommandError(f"File {filepath} doesn't exists!")

        logos = []
        with open(filepath) as file_handler:
            logos = json.load(file_handler)
        if len(logos) == 0:
            raise CommandError(f"File contains nothing!")

        for logo in logos:
            try:
                new_logo = Logo.objects.get(pk=logo["id"])
                self.stdout.write("U", ending="")
                new_logo.active = True
                new_logo.creator = logo["author"]
                new_logo.description = "" # not used on website
            except Logo.DoesNotExist:
                self.stdout.write("C", ending="")
                new_logo = Logo(
                    active = True,
                    creator = logo["author"],
                )
                new_logo.id = logo["id"]
            except Exception:
                self.stdout.write(self.style.ERROR("E"), ending="")
                continue

            # Loading logo file
            with open(filepath.parent / logo["filename"], "rb") as picture_file:
                new_logo.file.save(logo["filename"], File(picture_file))

            new_logo.save()

        self.stdout.write("")
        self.stdout.write(self.style.SUCCESS("Import finished"))
