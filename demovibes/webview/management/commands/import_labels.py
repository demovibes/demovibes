from datetime import datetime
import json
from pathlib import Path

from django.contrib.auth.models import User
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from webview.models import Artist, Label, Song


class Command(BaseCommand):
    help = "Imports labels from json file passed as argument"

    def add_arguments(self, parser):
        parser.add_argument("label_file", type=str)

    def handle(self, *args, **options):
        filepath = Path(options.get("label_file", "labels.json"))
        if not filepath.exists():
            raise CommandError(f"File {filepath} doesn't exists!")

        labels = []
        with open(filepath) as file_handler:
            labels = json.load(file_handler)
        if len(labels) == 0:
            raise CommandError(f"File contains nothing!")

        datation = "%d %b. %Y"
        for label in labels:
            try:
                found_date = datetime.strptime(label["found_date"], datation)
            except ValueError:
                found_date = None
            try:
                cease_date = datetime.strptime(label["shutdown_date"], datation)
            except ValueError:
                cease_date = None
            try:
                last_updated = datetime.strptime(label["last_updated"], datation)
            except ValueError:
                last_updated = None
            try:
                new_label = Label.objects.get(pk=label["id"])
                self.stdout.write("U", ending="")
                new_label.name = label["name"]
                new_label.info = label["description"]
                new_label.found_date = found_date
                new_label.cease_date = cease_date
                new_label.last_updated = last_updated
                new_label.startswith = label["name"][0].lower()
                new_label.status = "A"
                new_label.webpage = ""
                new_label.wiki_link = ""
            except Label.DoesNotExist:
                self.stdout.write("C", ending="")
                new_label = Label(
                    name = label["name"],
                    info = label["description"],
                    found_date = found_date,
                    cease_date = cease_date,
                    last_updated = last_updated,
                    startswith = label["name"][0].lower(),
                )
                new_label.id = label["id"]
            except Exception:
                self.stdout.write(self.style.ERROR("E"), ending="")
                continue

            # Loading logo file
            try:
                with open(filepath.parent / label["logo"], "rb") as picture_file:
                    new_label.logo.save(label["logo"], File(picture_file))
            except FileNotFoundError:
                # Lost logo :o/
                pass

            # Managing creator
            if label["added_by"] != "":
                user = User.objects.filter(username=label["added_by"]).first()
                if user is None:
                    user = User(username=label["added_by"])
                user.save()
                new_label.created_by = user

            # Managing website and wiki
            for link in label["links"]:
                if link["name"].lower() == "[wiki]":
                    new_label.wiki_link = link["url"]
                elif link["name"].lower() == "[website]":
                    new_label.webpage = link["url"]
                else:
                    self.stdout.write(self.style.ERROR(f"Link unknown: {link['name']}"))

            new_label.save()

            # Managing linked artists
            for artist in label["artists"]:
                handle = artist["name"].split("(")[0].strip()
                if "(" in artist["name"]:
                    name = artist["name"].split("(")[1].strip()[:-1]
                else:
                    name = ""
                try:
                    new_artist = Artist.objects.get(pk=artist["id"])
                except Artist.DoesNotExist:
                    new_artist = Artist(handle=handle)
                    new_artist.id = artist["id"]
                    new_artist.save()
                new_artist.handle = handle
                new_artist.name = name
                new_artist.labels.add(new_label)
                new_artist.save()
                
            # Managing linked songs
            # TODO: No metadata here, so no song
            # ~ for song in label["songs"]:
                # ~ try:
                    # ~ new_song = Song.objects.get(pk=song["id"])
                # ~ except Song.DoesNotExist:
                    # ~ new_song = Song(title=song["name"])
                    # ~ new_song.id = song["id"]
                # ~ new_song.save()
                
        self.stdout.write("")
        self.stdout.write(self.style.SUCCESS("Import finished"))
