import json
from pathlib import Path

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from webview.models import SongPlatform


class Command(BaseCommand):
    help = "Imports platforms from json file passed as argument"

    def add_arguments(self, parser):
        parser.add_argument("platform_file", type=str)

    def handle(self, *args, **options):
        filepath = Path(options.get("platform_file", "platforms.json"))
        if not filepath.exists():
            raise CommandError(f"File {filepath} doesn't exists!")

        platforms = []
        with open(filepath) as file_handler:
            platforms = json.load(file_handler)
        if len(platforms) == 0:
            raise CommandError(f"File contains nothing!")

        for platform in platforms:
            try:
                new_platform = SongPlatform.objects.get(pk=platform["id"])
                self.stdout.write("U", ending="")
                new_platform.title = platform["name"]
                new_platform.description = platform["description"]
            except SongPlatform.DoesNotExist:
                self.stdout.write("C", ending="")
                new_platform = SongPlatform(
                    title = platform["name"],
                    description = platform["description"],
                )
                new_platform.id = platform["id"]
            except Exception:
                self.stdout.write(self.style.ERROR("E"), ending="")
                continue

            # Loading symbol file
            with open(filepath.parent / platform["symbol"], "rb") as picture_file:
                new_platform.symbol.save(platform["symbol"], File(picture_file))
            # image file seems not used in website

            new_platform.save()

        self.stdout.write("")
        self.stdout.write(self.style.SUCCESS("Import finished"))
