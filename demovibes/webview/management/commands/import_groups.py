from datetime import datetime
import json
from pathlib import Path
import sys

from django.contrib.auth.models import User
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from webview.models import Artist, Group, Label, Song


class Command(BaseCommand):
    help = "Imports labels from json file passed as argument"

    def add_arguments(self, parser):
        parser.add_argument("group_file", type=str)

    def handle(self, *args, **options):
        filepath = Path(options.get("group_file", "groups.json"))
        if not filepath.exists():
            raise CommandError(f"File {filepath} doesn't exists!")

        groups = []
        with open(filepath) as file_handler:
            groups = json.load(file_handler)
        if len(groups) == 0:
            raise CommandError(f"File contains nothing!")

        datation = "%B %d, %Y"
        for group in groups:
            try:
                found_date = datetime.strptime(group["found_date"], "%B %d, %Y")
            except ValueError:
                found_date = None
            try:
                new_group = Group.objects.get(pk=group["group_id"])
                self.stdout.write("U", ending="")
                new_group.name = group["name"]
                new_group.info = group["info"]
                new_group.found_date = found_date
                new_group.pouetid = group["pouetid"] or None
                new_group.startswith = group["name"][0].lower()
                new_group.status = "A"
                new_group.webpage = group["website"]
            except Group.DoesNotExist:
                self.stdout.write("C", ending="")
                new_group = Group(
                    name = group["name"],
                    info = group["info"],
                    found_date = found_date,
                    pouetid = group["pouetid"] or None,
                    startswith = group["name"][0].lower(),
                    webpage = group["website"],
                )
                new_group.id = group["group_id"]
            except Exception:
                self.stdout.write(self.style.ERROR("E"), ending="")
                continue
            sys.stdout.flush()
            
            # Loading logo file, if necessary
            if "no_picture.png" not in group["logo"]:
                try:
                    with open(filepath.parent / group["logo"], "rb") as picture_file:
                        new_group.logo.save(group["logo"], File(picture_file))
                except FileNotFoundError:
                    # Lost logo :o/
                    pass

            # Managing creator
            # TODO: Get "Added by" information from website
            # ~ if label["added_by"] != "":
                # ~ user = User.objects.filter(username=label["added_by"]).first()
                # ~ if user is None:
                    # ~ user = User(username=label["added_by"])
                # ~ user.save()
                # ~ new_label.created_by = user

            new_group.save()

            # Managing linked artists
            for artist in group["artists"]:
                try:
                    new_artist = Artist.objects.get(pk=artist["id"])
                    new_artist.handle = artist["handle"]
                except Artist.DoesNotExist:
                    new_artist = Artist(handle=artist["handle"])
                    new_artist.id = artist["id"]
                    new_artist.save()
                new_artist.groups.add(new_group)
                new_artist.save()
                
            # Managing linked songs
            # TODO: No metadata here, so no song
            # ~ for song in label["songs"]:
                # ~ try:
                    # ~ new_song = Song.objects.get(pk=song["id"])
                # ~ except Song.DoesNotExist:
                    # ~ new_song = Song(title=song["name"])
                    # ~ new_song.id = song["id"]
                # ~ new_song.save()

        self.stdout.write("")
        self.stdout.write(self.style.SUCCESS("Import finished"))
