from django.urls import include, path
from . import views


# ~ """
# ~ Queries pending labels, needs to be all or pending wont show
# ~ """
# ~ labels_a_dict = {
# ~ 'queryset': Label.objects.all(),
# ~ }


# ~ """
# ~ Retreive all FAQ Questions marked as 'Active'
# ~ """
# ~ faq_dict = {
# ~ 'queryset': Faq.objects.filter(active=True),
# ~ }

app_name = "webview"
urlpatterns = [
    # Generic areas
    path("", views.NewsView.as_view(), name="root"),
    path("about/", views.site_about, name="about"),
    # Artists
    path("artists/", views.ListArtists(), name="artists"),
    path("artists/<str:letter>/", views.ListArtists(), name="artists_letter"),
    path("artist/<int:pk>/", views.ArtistView.as_view(), name="artist"),
    path("artist/<int:artist_id>/upload/", views.upload_song, name="upload"),
    path("artist/create/", views.create_artist, name="createartist"),
    path("new_artists/", views.activate_artists, name="newartists"),
    # Chat
    path("chat/", views.chat, name="chat"),
    # Compilations
    path("compilations/", views.ListComilations(), name="compilations"),
    path("compilations/new/", views.AddCompilation(), name="newcomp"),
    path(
        "compilations/<str:letter>/",
        views.ListComilations(),
        name="compilations_letter",
    ),
    path(
        "compilation/<int:compilation_id>/screenshot/",
        views.CompilationAddScreenshot(),
        name="add_comp_screenshot",
    ),
    path("compilation/<int:comp_id>/", views.view_compilation, name="compilation"),
    path(
        "compilation/<int:comp_id>/edit/",
        views.EditCompilation(),
        name="compilation-edit",
    ),
    path("new_compilations/", views.activate_compilations, name="newcompilations"),
    # FAQ
    path("faq/", views.FaqsView.as_view(), name="faq"),
    path("faq/<int:pk>/", views.FaqView.as_view(), name="faqitem"),
    # Favorites
    path("favorites/", views.list_favorites, name="favorites"),
    path("favorites/change/", views.ChangeFavorite(), name="change_fav"),
    # path("favorites/add/<int:id>/", views.add_favorite, name = "add_fav"),
    # path("favorites/del/<int:id>/", views.del_favorite, name = "del_fav"),
    # Groups
    path("groups/", views.ListGroups(), name="groups"),
    path("groups/<str:letter>/", views.ListGroups(), name="groups_letter"),
    path("group/<int:pk>/", views.GroupView.as_view(), name="group"),
    path("group/create/", views.create_group, name="creategroup"),
    path("new_groups/", views.activate_groups, name="newgroups"),
    # Inbox
    path("inbox/", views.inbox, name="inbox"),
    path("inbox/<int:pm_id>/", views.read_pm, name="read_pm"),
    path("inbox/send/", views.send_pm, name="send_pm"),
    # Labels
    path("labels/", views.ListLabels(), name="labels"),
    path("label/<int:pk>/", views.LabelView.as_view(), name="label"),
    path("labels/<str:letter>/", views.ListLabels(), name="labels_letter"),
    path("label/create/", views.create_label, name="createlabel"),
    path("new_labels/", views.activate_labels, name="newlabels"),
    # Licenses
    path("licenses/", views.LicenseList(), name="licenses"),
    path("license/<int:pk>/", views.License(), name="license"),
    # Links
    path("links/", views.site_links, name="links"),
    path("links/<slug:slug>/", views.link_category, name="linkcategory"),
    path("link/create/", views.link_create, name="createlink"),
    path("link/pending/", views.activate_links, name="newlinks"),
    # Metainfos
    path("metainfo/<int:songinfo_id>/", views.view_songinfo, name="songinfo-view"),
    # Online
    path("online/", views.users_online, name="users_online"),
    # Oneliner
    path("oneliner/", views.NewsView.as_view(), name="oneliner"),
    path("oneliner/submit/", views.oneliner_submit, name="oneliner_submit"),
    path("oneliner/mute/", views.MuteOneliner(), name="muteoneliner"),
    path("oneliner/lookie/", views.OnelinerHistorySearch(), name="onelinerhsearch"),
    # Play
    path("play/", views.play_stream, name="play_stream"),
    # path("play/", django.views.generic.simple.direct_to_template, {'template':'webview/radioplay.html'}, name = "play_stream"),
    # Platforms
    path("platforms/", views.PlatformsView.as_view(), name="platforms"),
    path("platform/<int:pk>/", views.PlatformView.as_view(), name="platform"),
    # Queue
    path("queue/", views.listQueue(), name="queue"),
    # Recent
    path("recent/", views.show_approvals, name="recent"),
    # Search
    # path("search/", views.search, name = "search"),
    # Screenshots
    path("screenshots/", views.ListScreenshots(), name="screenshots"),
    path(
        "screenshots/<str:letter>/", views.ListScreenshots(), name="screenshots_letter"
    ),
    path("screenshot/<int:screenshot_id>/", views.list_screenshot, name="screenshot"),
    path("screenshot/create/", views.create_screenshot, name="createscreenshot"),
    path(
        "screenshot/<int:screenshot_id>/rethumb/",
        views.rebuild_thumb,
        name="screenshot-rethumb",
    ),
    path("new_screenshots/", views.activate_screenshots, name="newscreenshots"),
    # Smileys
    path("smileys/", views.ListSmileys(), name="smileys"),
    # Songs
    path("newshit/", views.new_songinfo_list, name="new-info"),
    path("songs/", views.ListSongs(), name="songs"),
    path("songs/<str:letter>/", views.ListSongs(), name="songs_letter"),
    path("song/vote/", views.VoteSong(), name="formvote"),
    path("song/queue/", views.QueueSong(), name="queue-song"),
    path("song/<int:song_id>/", views.list_song, name="song"),
    path("song/<int:song_id>/download/", views.DownloadSong(), name="dl-song"),
    path("song/<int:song_id>/addlink/", views.add_songlinks, name="song-addlink"),
    path("song/<int:song_id>/edit/", views.edit_songinfo, name="song-edit"),
    path(
        "song/<int:song_id>/infolist/",
        views.list_songinfo_for_song,
        name="song-infolist",
    ),
    path("song/<int:song_id>/comments/", views.songComments(), name="song_comment"),
    path("song/<int:song_id>/tags/", views.tagEdit(), name="songtags"),
    path("song/<int:song_id>/votes/", views.songVotes(), name="song_votes"),
    path("song/<int:song_id>/queue_history/", views.songHistory(), name="song_history"),
    path(
        "song/<int:song_id>/screenshot/",
        views.SongAddScreenshot(),
        name="add_screenshot",
    ),
    path("song/<int:song_id>/play/", views.PlaySong(), name="play_song"),
    path("comment/add/<int:song_id>/", views.addComment(), name="addcomment"),
    # Sources
    path("sources/", views.SourcesView.as_view(), name="sources"),
    path("source/<int:pk>/", views.SourceView.as_view(), name="source"),
    # Statistics
    path("statistics/", views.songStatistics(), name="stats-index"),
    path("statistics/<str:stattype>/", views.songStatistics(), name="stats"),
    # ~ # Statistics & Cache stuff
    path("spammers/", views.listUsers(), name="spammerlist"),
    path("spammers/ctest/", views.test_captcha, name="spammerlist-captcha"),
    path(
        "status/cache/", views.memcached_status, name="memcached"
    ),  # Show memcached status
    # Streams
    path("streams/", views.StreamView.as_view(), name="streams"),
    path("streams/streams.txt", views.StreamTxtView.as_view(), name="streams.txt"),
    path("streams/streams.m3u", views.StreamM3uView.as_view(), name="streams.m3u"),
    # Tags
    path("tags/", views.tagCloud(), name="tagcloud"),
    path("tags/<str:tag>/", views.tagDetail(), name="tagdetail"),
    # Themes
    path("themes/", views.ThemeList(), name="themelist"),
    path("theme/<int:theme_id>/", views.ThemeInfo(), name="themeinfo"),
    path(
        "theme/<int:theme_id>/add_image/", views.ThemeAddImage(), name="themeaddimage"
    ),
    path("theme/<int:theme_id>/edit/", views.ThemeEdit(), name="themeedit"),
    # Updated Informations
    path("updates/", views.showRecentChanges, name="updates"),
    path("uploaded_songs/", views.activate_upload, name="uploads"),
    path("upload_progress/", views.upload_progress, name="upload-progress"),
    # Users
    path("user/", views.MyProfile(), name="my_profile"),
    path("user/<str:user>/", views.ViewProfile(), name="profile"),
    path("user/<str:user>/favorites/", views.ViewUserFavs(), name="user-favs"),
    path("ajax/", include("webview.ajax_urls")),
    path("xml/", include("webview.xml_urls")),
]

# ~ if site_supports_song_file_replacements_by_user():
# ~ urlpatterns += [
# ~ path("song/<int:song_id>/upload/", views.upload_song_file, name='replace-song-file')
# ~ ]
