from haystack import indexes
from django.conf import settings
from webview.models import Artist, Group, Song, Userprofile


class ArtistIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    render = indexes.CharField(indexed=False, use_template=True)
    country = indexes.CharField(model_attr="home_country")

    def get_model(self):
        return Artist

    def index_queryset(self):
        return self.get_model().objects.all()

    def get_updated_field(self):
        return "last_updated"


class SongIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    render = indexes.CharField(indexed=False, use_template=True)

    # Need haystack 1.2 for this
    # title = CharField(model_attr='title', boost=5.0)

    def get_model(self):
        return Song

    def index_queryset(self):
        return self.get_model().objects.all()

    def get_updated_field(self):
        return "last_changed"

    # Need haystack 1.2 for this
    def prepare(self, obj):
        data = super(SongIndex, self).prepare(obj)
        # Boost is not working on whoosh
        if obj.status == "A" and settings.HAYSTACK_SEARCH_ENGINE != "whoosh":
            data["boost"] = 5.0
        return data


class GroupIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    render = indexes.CharField(indexed=False, use_template=True)

    def get_model(self):
        return Group

    def index_queryset(self):
        return self.get_model().objects.all()

    def get_updated_field(self):
        return "last_updated"


class UserIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    render = indexes.CharField(indexed=False, use_template=True)

    def get_model(self):
        return Userprofile

    def index_queryset(self):
        return self.get_model().objects.filter(visible_to="A")

    def get_updated_field(self):
        return "last_changed"
