from django.conf.urls import url
from django.contrib.auth.models import User

from webview.models import Song
from webview import xml_views


song_dict = {
    "queryset": Song.objects.all(),
    "mimetype": "application/xml",
    "template_name": "webview/xml/song.xml",
}

user_dict = {
    "queryset": User.objects.all(),
    "mimetype": "application/xml",
    "template_name": "webview/xml/user.xml",
}


urlpatterns = [
    url(r"^queue/$", xml_views.queue),
    url(r"^oneliner/$", xml_views.oneliner, name="xml-oneliner"),
    url(r"^oneliner/cache/$", xml_views.oneliner, name="xml-oneliner-cache"),
    url(r"^online/$", xml_views.online),
    url(r"^song/(?P<songid>\d+)/$", xml_views.SongInfo()),
    url(r"^compilation/(?P<pk>\d+)/$", xml_views.CompilationView.as_view()),
    url(r"^group/(?P<pk>\d+)/$", xml_views.GroupView.as_view()),
    url(r"^artist/(?P<pk>\d+)/$", xml_views.ArtistView.as_view()),
    url(r"^streams/$", xml_views.StreamsView.as_view()),
    # url(r'^artist/$',     artist),
    url(r"^user/(?P<username>\w+)/$", xml_views.UserInfo()),
    url(r"^user/(?P<username>\w+)/favorites/$", xml_views.UserFavorites()),
]
