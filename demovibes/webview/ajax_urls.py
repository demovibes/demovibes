from django.urls import path
from .models import *
from . import ajax_views

urlpatterns = [
    path("ping/<int:event_id>/", ajax_views.ping),
    path("monitor/<int:event_id>/", ajax_views.monitor),
    path("license/<int:id>/", ajax_views.LicenseView()),
    path("nowplaying/", ajax_views.nowplaying, name="ax-nowplaying"),
    path("queue/", ajax_views.queue, name="ax-queue"),
    path("history/", ajax_views.history, name="ax-history"),
    path("smileys/", ajax_views.smileys, name="ax-smileys"),
    path("songinfo/", ajax_views.songinfo, name="ax-songinfo"),
    path("oneliner/", ajax_views.oneliner, name="ax-oneliner"),
    path("tags/", ajax_views.get_tags, name="ax-taglist"),
    path("a_queue_<int:song_id>/", ajax_views.songupdate),
    path("words/<str:prefix>/", ajax_views.words),
    path("oneliner_submit/", ajax_views.oneliner_submit, name="ax-oneliner_submit"),
]
