from django.shortcuts import render
from baseview import BaseView as BV


class MyBaseView(BV):
    __name__ = "MyBaseView"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__qualname__ = "BaseView.MyBaseView"

    def render_template(self, template, context, request):
        return render(request, template, context, content_type="text/html")

    def deny_permission(self):
        return render(
            self.request,
            "base/error.html",
            {"error": "Sorry, you're not allowed to see this"},
        )
