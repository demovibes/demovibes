# Static files

_static_ folder aims to get all files used on the website.


## root

On root of _static_ folder, there are a lot of icons (gif/png) files. There are also two
jpg files and a txt file. We need to list which ones are really used and sort them in a
specific subfolder.


## emoticons

looooooots of smileys...


## flags

two letters countries flags but also some more : necta, europe.


## flash

Some flash files. Clearly too old junk :o)


## javamod

a jar file ?


## js

All js files for website.


## logos

Default logo. Logically, uploaded logos should be stored in _media_ folder.


## openid

Static files for openid_provider application I presume.


## themes

Some css and some folders, one for each available theme.


## tmp

Some garbage to clean
