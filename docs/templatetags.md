# Template tags

Demovibes uses some template tags into templates. Maybe some are useless now by using
jinja2 builtin filters and tags.

TO COMPLETE...


## ankh filter


## user_based_filter


## antiankh_filter


## BetterPaginator


## paginate

## j_count_text_links

## get_banner_links

## pending

## get_button_links


## current_song


## get_roneliner


## get_css_for_user


## user_css


## get_news


## GetSongRatingNode


## GetSongRatingStarsAvgNode


## get_text_link_entries


## GetTextLinkEntries


## CountTextLinkEntries


## GetCss


## j_get_post_count


## GetPostCount


## get_unread_count


## get_pm_subject_suggestion


## GetInboxNode


## SidebarNewsNode


## StreamListNode


## GetVoteNode


## IsFavoriteNode


## get_song_queue_tag_j


## get_song_queue_tag


## GetSongQueueTag

## smiley_general

## custom_filters


## make_smileys


## reify


## BBCode

### bb_artist


### bb_queue


### bb_song


### get_flag_path


### bb_theme


### bb_flag

### bb_user

### bb_artistname

### bb_group

### bb_groupname

### bb_label

### bb_labelname

### bb_platform

### bb_platformname

### bb_thread

### bb_forum

### bb_size

### bb_youtube

### bb_youtube2

### bb_compilation

### bb_compilation_name

### bb_faq

### bb_youtube_ol

### bb_googlevideo_ol

### bb_youtube_name_ol

### bb_gvideo

### bb_ankhify


## Tags

### site_name


### site_url


### get_online_users


### get_rating_stars_song_avg


### get_text_links


### count_text_links


### logo


### ajaxevent


### get_oneliner


### get_post_count


### css


### get_inbox


### get_vote


### get_song_rating


### get_song_queuetag


### get_sidebar_news


### get_streams


### is_favorite


## Filters

### oneliner_mediaparse


### removesmileys


### bbcode


### bbcode_oneliner


### wordwrap


### smileys_oneliner


### smileys_restricted


### smileys


### flag


### getattrs


### ankhalize


### dv_urlize

