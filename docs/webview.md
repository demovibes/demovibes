# Webview application

webview application is the core of demovibes website.

It manages a lot of things, that means too much. It should be cut into pieces like this:

* FAQ : the FAQ
* Music : everything related to songs and artists
* Queue : managing queue
* Votes : favourites and votes
* News : everything related to news, on homepage and on sidebar
* Oneliner : the infamous one
* Themes : managing themes
* Inbox : for communication between users


## Template tags

Demovibes uses some template tags into templates. Maybe some are useless now by using
jinja2 builtin filters and tags.

See [templatetags documentation](templatetags.md)


## Ajax and XML views

Some views are separated into two categories : Ajax and XML.

XML views should be moved in some sort of API views (XML and JSON I think).

Ajax views should stay in common views, even if they serve only datas (no HTML).


## averages

This script can be used to create a _votes.html_ containing vote average of all users.

Used nowhere, useless script.


## common

Common is a library of usefull functions. It has to be analyzed.


## dscan

Script used to scna music files with demosauce tool.


## protected downloads

Some protection agains downloading songs. Useless today, because you just have to put
_media_ folder somewhere unreachable from the internet.


## search indexes

Indexes for haystack.


## song locktime

Computation on queuing a song based on its votes. Has some tests directly in it, this
should be moved somewhere else.

Version 2 is _different_.

Song queuing is a recurent problem on Nectarine radio :o)


## upload progress

Some script to check upload progression for a file.

Not used ?


## Management commands

Only one command : load fixtures.

Fixtures are for testing. Doubt if this is still used or of any interest as present
fixtures are for some "seleinum" testing and we don't have any selenium scenario.


