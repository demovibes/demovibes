# Demovibes documentation

## Installation

Installation was done via some Vagrant thing which I absolutely don't know. Beside, I
don't know how actual website is running.

For now, installation is for development only so it'll be through _venv_ and _pip_:

    git clone https://framagit.org/demovibes/demovibes
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements/dev3.txt
    python manage.py migrate
    python manage.py createsuperuser

And to run it, well, it's a Django application:

    python manage.py runserver



## Administration

Administration is done via the good old Django's admin website. Nothing more to say for
now.


## Registration

Users can register via the "Create account" link. They should receive an email (not
tested yet) with an ctivation link.

Once they activated their account, they still can't connect : an admin has to validate
each account.

Apparently, a lot of spam used to be there at the time the website was built.

See [Registration documentation](registration.md)


## Forum

Forum is an application used to discuss between site's users. It should be extracted
from demovibes to an independant application.


## IP2CC

IP2CC means IP to Country code. It's a package from "Python Publishing Accessories", a
library of python tools not updated since 2014.

It is used in webview.models.


## cprofiler

cprofiler is a middleware used to _profile_ a webpage while in debug mode.

This tool can be thrown away.


## openid_provider

Django OpenID Provider v0.2. is used to make your django website an OpenID provider.


## search

A specific application for searching with haystack.


## webview

Main application, providing all datas for website.

See [Webview documentation](webview.md)


## settings

Settings is largely commented and we can see traces of older uses. It needs to be
cleaned and to delimit clearly specific zones for each application.


## uwsgi events

Some kind of events handler written in bottle.

Should rewrite this part with some kind of socket in Django.


## eventful

Some kind of messenger ?


## Static files

There is a bunch of files in static folder. They should be cleaned too.

See [Static documentation](static.md)



## Third parties

Going through all files, we can make a list of third party libs and projects used by
demovibes:

* Flash streaming
* uWSGI event server
* Haystack
* Database (postgreSQL or MySQL)
* Nginx
* SMTP for sending emails
* Cherokee
* demosauce (icecast client)
* memcached
* jinja2
* IceS
* IceCast2
* bit.ly
* twitter



