# Documentation

## What is this all about ?

This documentation tries to list and document (eh eh) all stuff for demovibes project.


## Folders

### contrib

Documents for set up a new instance of demovibes.

### demovibes

Main project, an engine for streaming music.

See [Demovibes documentation](demovibes.md)

### docs

This documentation!

### eventful

Some kind of client/server messaging. Dunno what for.

### requirements

Python/pip requirements for demovibes.

Use only _prod.txt_ for prod, and _dev3.txt_ for development. All others requirements
are automatically included as necessary.

/!\ Production state NOT READY during the upgrade of source code.

### static

Static files for demovibes.

### templates

Templates for demovibes.

### uwsgi_events

Some kind of second application to manage demovibes events.
